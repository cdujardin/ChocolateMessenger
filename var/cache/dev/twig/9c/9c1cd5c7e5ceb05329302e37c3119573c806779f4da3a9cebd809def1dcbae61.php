<?php

/* :Message:show.html.twig */
class __TwigTemplate_6849936ed0d231f13530ea5150a45a528f3cade5381bf8b9a9e42af183697143 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Message:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_551df556580e551033aa6f07b9330e28d8f6b5a00ecfd387c2526fa13ff9d8e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_551df556580e551033aa6f07b9330e28d8f6b5a00ecfd387c2526fa13ff9d8e8->enter($__internal_551df556580e551033aa6f07b9330e28d8f6b5a00ecfd387c2526fa13ff9d8e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:show.html.twig"));

        $__internal_dffb9f8de393867b91728af81b0b6ee5f71f005b0d2e5f26e9381a03df190b8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dffb9f8de393867b91728af81b0b6ee5f71f005b0d2e5f26e9381a03df190b8b->enter($__internal_dffb9f8de393867b91728af81b0b6ee5f71f005b0d2e5f26e9381a03df190b8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_551df556580e551033aa6f07b9330e28d8f6b5a00ecfd387c2526fa13ff9d8e8->leave($__internal_551df556580e551033aa6f07b9330e28d8f6b5a00ecfd387c2526fa13ff9d8e8_prof);

        
        $__internal_dffb9f8de393867b91728af81b0b6ee5f71f005b0d2e5f26e9381a03df190b8b->leave($__internal_dffb9f8de393867b91728af81b0b6ee5f71f005b0d2e5f26e9381a03df190b8b_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dc8da2650422cef50ffcc1000fa0d283afb35449bafcdd3fbb92a4c1cf1aa2aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc8da2650422cef50ffcc1000fa0d283afb35449bafcdd3fbb92a4c1cf1aa2aa->enter($__internal_dc8da2650422cef50ffcc1000fa0d283afb35449bafcdd3fbb92a4c1cf1aa2aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8c9b813fa6e860ea7b34a85ff405b0186cf81acef9e74238e53255814c4bc4df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c9b813fa6e860ea7b34a85ff405b0186cf81acef9e74238e53255814c4bc4df->enter($__internal_8c9b813fa6e860ea7b34a85ff405b0186cf81acef9e74238e53255814c4bc4df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1 class=\"title\">Message \"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "object", array()), "html", null, true);
        echo "\"</h1>


    <div class=\"container\">
      <div class=\"row message\">
        <div class=\"col-xs-9\">
          <p>Object : ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "object", array()), "html", null, true);
        echo "</p>
          <p>Message : ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "message", array()), "html", null, true);
        echo "</p>
          <p>Author : ";
        // line 12
        if ($this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "author", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "author", array()), "firstname", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "author", array()), "lastname", array()), "html", null, true);
            echo ")";
        }
        echo "</p>
        </div>
        <div class=\"col-xs-3 icon\">
          <a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_index");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Return\"/></a>
          <a  href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_edit", array("id" => $this->getAttribute(($context["message"] ?? $this->getContext($context, "message")), "id", array()))), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/modify.png"), "html", null, true);
        echo "\" alt=\"Modifier\"/></a>
          ";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "<input class=\"icon-delete\" type=\"image\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/remove.png"), "html", null, true);
        echo "\" alt=\"effacer\"/>";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "

        </div>
      </div>
    </div>

";
        
        $__internal_8c9b813fa6e860ea7b34a85ff405b0186cf81acef9e74238e53255814c4bc4df->leave($__internal_8c9b813fa6e860ea7b34a85ff405b0186cf81acef9e74238e53255814c4bc4df_prof);

        
        $__internal_dc8da2650422cef50ffcc1000fa0d283afb35449bafcdd3fbb92a4c1cf1aa2aa->leave($__internal_dc8da2650422cef50ffcc1000fa0d283afb35449bafcdd3fbb92a4c1cf1aa2aa_prof);

    }

    public function getTemplateName()
    {
        return ":Message:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 17,  84 => 16,  78 => 15,  67 => 12,  63 => 11,  59 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1 class=\"title\">Message \"{{ message.object }}\"</h1>


    <div class=\"container\">
      <div class=\"row message\">
        <div class=\"col-xs-9\">
          <p>Object : {{ message.object }}</p>
          <p>Message : {{ message.message }}</p>
          <p>Author : {% if message.author %}{{message.author.firstname}} ({{message.author.lastname}}){% endif %}</p>
        </div>
        <div class=\"col-xs-3 icon\">
          <a href=\"{{ path('message_index') }}\"><img src=\"{{ asset('img/return.png') }}\" alt=\"Return\"/></a>
          <a  href=\"{{ path('message_edit', { 'id': message.id }) }}\"><img src=\"{{ asset('img/modify.png') }}\" alt=\"Modifier\"/></a>
          {{ form_start(delete_form) }}<input class=\"icon-delete\" type=\"image\" src=\"{{ asset('img/remove.png') }}\" alt=\"effacer\"/>{{ form_end(delete_form) }}

        </div>
      </div>
    </div>

{% endblock %}
", ":Message:show.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Message/show.html.twig");
    }
}
