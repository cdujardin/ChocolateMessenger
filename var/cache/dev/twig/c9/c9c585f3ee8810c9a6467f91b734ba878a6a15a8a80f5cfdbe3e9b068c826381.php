<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_3bad7450074ba7743855011c96717adf4a3b177996b5c74cc2aa439f6c0a2c28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe67df2f11ff5160d2c57b5a65bb6aeff50c06d5199ce0758d78ce4b15ea79eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe67df2f11ff5160d2c57b5a65bb6aeff50c06d5199ce0758d78ce4b15ea79eb->enter($__internal_fe67df2f11ff5160d2c57b5a65bb6aeff50c06d5199ce0758d78ce4b15ea79eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_6215c33d8b613defe8b859f344786f1909a8c49f0d8b8f1ba0bee659ff7dbc45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6215c33d8b613defe8b859f344786f1909a8c49f0d8b8f1ba0bee659ff7dbc45->enter($__internal_6215c33d8b613defe8b859f344786f1909a8c49f0d8b8f1ba0bee659ff7dbc45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_fe67df2f11ff5160d2c57b5a65bb6aeff50c06d5199ce0758d78ce4b15ea79eb->leave($__internal_fe67df2f11ff5160d2c57b5a65bb6aeff50c06d5199ce0758d78ce4b15ea79eb_prof);

        
        $__internal_6215c33d8b613defe8b859f344786f1909a8c49f0d8b8f1ba0bee659ff7dbc45->leave($__internal_6215c33d8b613defe8b859f344786f1909a8c49f0d8b8f1ba0bee659ff7dbc45_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
