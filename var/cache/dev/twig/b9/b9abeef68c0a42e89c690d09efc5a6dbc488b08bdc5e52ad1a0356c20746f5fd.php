<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_150ca7d995468e02789c19d70f5a82365301ecebc21b38fde8bc2214e9f04740 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b41b9202cff439ed85b9585af04802cd946f38841e974e39fa8a8155830f0adc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b41b9202cff439ed85b9585af04802cd946f38841e974e39fa8a8155830f0adc->enter($__internal_b41b9202cff439ed85b9585af04802cd946f38841e974e39fa8a8155830f0adc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_aa026315a774444c32b42a1ba67304ec4942f40870dfbbc254d9cdca27f2379a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa026315a774444c32b42a1ba67304ec4942f40870dfbbc254d9cdca27f2379a->enter($__internal_aa026315a774444c32b42a1ba67304ec4942f40870dfbbc254d9cdca27f2379a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b41b9202cff439ed85b9585af04802cd946f38841e974e39fa8a8155830f0adc->leave($__internal_b41b9202cff439ed85b9585af04802cd946f38841e974e39fa8a8155830f0adc_prof);

        
        $__internal_aa026315a774444c32b42a1ba67304ec4942f40870dfbbc254d9cdca27f2379a->leave($__internal_aa026315a774444c32b42a1ba67304ec4942f40870dfbbc254d9cdca27f2379a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_9d8fb3578323cf9def460daa5aeba8803f92f4f9daac5c56b713910b2a228173 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d8fb3578323cf9def460daa5aeba8803f92f4f9daac5c56b713910b2a228173->enter($__internal_9d8fb3578323cf9def460daa5aeba8803f92f4f9daac5c56b713910b2a228173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_fb3881e6bcc294b85386bf12deab4d5f278fcb5430267d2f4b117e42ae58e931 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb3881e6bcc294b85386bf12deab4d5f278fcb5430267d2f4b117e42ae58e931->enter($__internal_fb3881e6bcc294b85386bf12deab4d5f278fcb5430267d2f4b117e42ae58e931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_fb3881e6bcc294b85386bf12deab4d5f278fcb5430267d2f4b117e42ae58e931->leave($__internal_fb3881e6bcc294b85386bf12deab4d5f278fcb5430267d2f4b117e42ae58e931_prof);

        
        $__internal_9d8fb3578323cf9def460daa5aeba8803f92f4f9daac5c56b713910b2a228173->leave($__internal_9d8fb3578323cf9def460daa5aeba8803f92f4f9daac5c56b713910b2a228173_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_3fc12dbd49706bf593558131edb3039a72f6e087bb19f7573a0e63f019d6de20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fc12dbd49706bf593558131edb3039a72f6e087bb19f7573a0e63f019d6de20->enter($__internal_3fc12dbd49706bf593558131edb3039a72f6e087bb19f7573a0e63f019d6de20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f0c7e0f27d016ab34fd19c905e2bbc6b197c7dc4bad0ba646d7cd01bee81212c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0c7e0f27d016ab34fd19c905e2bbc6b197c7dc4bad0ba646d7cd01bee81212c->enter($__internal_f0c7e0f27d016ab34fd19c905e2bbc6b197c7dc4bad0ba646d7cd01bee81212c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_f0c7e0f27d016ab34fd19c905e2bbc6b197c7dc4bad0ba646d7cd01bee81212c->leave($__internal_f0c7e0f27d016ab34fd19c905e2bbc6b197c7dc4bad0ba646d7cd01bee81212c_prof);

        
        $__internal_3fc12dbd49706bf593558131edb3039a72f6e087bb19f7573a0e63f019d6de20->leave($__internal_3fc12dbd49706bf593558131edb3039a72f6e087bb19f7573a0e63f019d6de20_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
