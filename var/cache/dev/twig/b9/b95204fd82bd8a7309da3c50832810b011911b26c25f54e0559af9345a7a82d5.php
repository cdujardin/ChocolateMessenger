<?php

/* :Author:new.html.twig */
class __TwigTemplate_451778d710d32535f1893743c7c160876e3daa4eab541238daf44dd8efb1cff5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Author:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b41170c0754ac4984d2f66a621ab82468c7c8f56f9a3f70cca851e44b15f7555 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b41170c0754ac4984d2f66a621ab82468c7c8f56f9a3f70cca851e44b15f7555->enter($__internal_b41170c0754ac4984d2f66a621ab82468c7c8f56f9a3f70cca851e44b15f7555_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:new.html.twig"));

        $__internal_2c0f3321252436373f52611f884668a4092a6ed92fd5d52ae0bd3f7a7362b6dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c0f3321252436373f52611f884668a4092a6ed92fd5d52ae0bd3f7a7362b6dc->enter($__internal_2c0f3321252436373f52611f884668a4092a6ed92fd5d52ae0bd3f7a7362b6dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:new.html.twig"));

        // line 2
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["form"] ?? $this->getContext($context, "form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b41170c0754ac4984d2f66a621ab82468c7c8f56f9a3f70cca851e44b15f7555->leave($__internal_b41170c0754ac4984d2f66a621ab82468c7c8f56f9a3f70cca851e44b15f7555_prof);

        
        $__internal_2c0f3321252436373f52611f884668a4092a6ed92fd5d52ae0bd3f7a7362b6dc->leave($__internal_2c0f3321252436373f52611f884668a4092a6ed92fd5d52ae0bd3f7a7362b6dc_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_52a3add40cd0dfa105c9b71fc41de70df20c9cb4eac13154db18992ec70ffa5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52a3add40cd0dfa105c9b71fc41de70df20c9cb4eac13154db18992ec70ffa5e->enter($__internal_52a3add40cd0dfa105c9b71fc41de70df20c9cb4eac13154db18992ec70ffa5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_044b3ad9ecbde13c01df3743ca4d0ffeaeb219fa9a27620798b8f18a8c86fdd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_044b3ad9ecbde13c01df3743ca4d0ffeaeb219fa9a27620798b8f18a8c86fdd6->enter($__internal_044b3ad9ecbde13c01df3743ca4d0ffeaeb219fa9a27620798b8f18a8c86fdd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<!--Form add / Edit -->
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-xs-12\">
        <h1 class=\"title\">Create author </h1>
        ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
          ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
          <div class=\"row btnedit\">
            <div class=\"col-xs-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Create\" />
            </div>

            <div class=\"col-xs-2 icon\"/>
              <a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_index");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Return\"/></a>
            </div>
          </div>
      ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
  </div>
</div>

";
        
        $__internal_044b3ad9ecbde13c01df3743ca4d0ffeaeb219fa9a27620798b8f18a8c86fdd6->leave($__internal_044b3ad9ecbde13c01df3743ca4d0ffeaeb219fa9a27620798b8f18a8c86fdd6_prof);

        
        $__internal_52a3add40cd0dfa105c9b71fc41de70df20c9cb4eac13154db18992ec70ffa5e->leave($__internal_52a3add40cd0dfa105c9b71fc41de70df20c9cb4eac13154db18992ec70ffa5e_prof);

    }

    public function getTemplateName()
    {
        return ":Author:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 22,  74 => 19,  64 => 12,  60 => 11,  52 => 5,  43 => 4,  33 => 1,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% form_theme form 'bootstrap_3_layout.html.twig' %}

{% block body %}

<!--Form add / Edit -->
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-xs-12\">
        <h1 class=\"title\">Create author </h1>
        {{ form_start(form) }}
          {{ form_widget(form) }}
          <div class=\"row btnedit\">
            <div class=\"col-xs-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Create\" />
            </div>

            <div class=\"col-xs-2 icon\"/>
              <a href=\"{{ path('author_index') }}\"><img src=\"{{ asset('img/return.png') }}\" alt=\"Return\"/></a>
            </div>
          </div>
      {{ form_end(form) }}
    </div>
  </div>
</div>

{% endblock %}
", ":Author:new.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Author/new.html.twig");
    }
}
