<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_9553fa96834c0ab4dcb8de3e70f72131636b26386dd26bde3b020531765d74b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9523d351f524cc2f3072f2e1f9fe2a3d36dc7f3f7e0f5df735bffc78155260ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9523d351f524cc2f3072f2e1f9fe2a3d36dc7f3f7e0f5df735bffc78155260ea->enter($__internal_9523d351f524cc2f3072f2e1f9fe2a3d36dc7f3f7e0f5df735bffc78155260ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_ae0b60ec563816df8fd703ca84a1104b9b02805113cd6ae4feef408154837df3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae0b60ec563816df8fd703ca84a1104b9b02805113cd6ae4feef408154837df3->enter($__internal_ae0b60ec563816df8fd703ca84a1104b9b02805113cd6ae4feef408154837df3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_9523d351f524cc2f3072f2e1f9fe2a3d36dc7f3f7e0f5df735bffc78155260ea->leave($__internal_9523d351f524cc2f3072f2e1f9fe2a3d36dc7f3f7e0f5df735bffc78155260ea_prof);

        
        $__internal_ae0b60ec563816df8fd703ca84a1104b9b02805113cd6ae4feef408154837df3->leave($__internal_ae0b60ec563816df8fd703ca84a1104b9b02805113cd6ae4feef408154837df3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
