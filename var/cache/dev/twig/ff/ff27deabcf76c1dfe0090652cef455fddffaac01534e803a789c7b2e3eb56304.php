<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_c7cb01721a01044236d7c4aee55b1d09763d538a1adc24252dbbc4e85b4d2f73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_250809c1a090d8d903c468bc488e12807dd91d9351c62fd560c257b3782215d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_250809c1a090d8d903c468bc488e12807dd91d9351c62fd560c257b3782215d7->enter($__internal_250809c1a090d8d903c468bc488e12807dd91d9351c62fd560c257b3782215d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_fd0c344a7bf4ba2b2e96f9eab28dff89f77db0222ad0ef5902a139ac92ebe6e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd0c344a7bf4ba2b2e96f9eab28dff89f77db0222ad0ef5902a139ac92ebe6e3->enter($__internal_fd0c344a7bf4ba2b2e96f9eab28dff89f77db0222ad0ef5902a139ac92ebe6e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_250809c1a090d8d903c468bc488e12807dd91d9351c62fd560c257b3782215d7->leave($__internal_250809c1a090d8d903c468bc488e12807dd91d9351c62fd560c257b3782215d7_prof);

        
        $__internal_fd0c344a7bf4ba2b2e96f9eab28dff89f77db0222ad0ef5902a139ac92ebe6e3->leave($__internal_fd0c344a7bf4ba2b2e96f9eab28dff89f77db0222ad0ef5902a139ac92ebe6e3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
