<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_bbc52713541d035994e8865cb299655a6527c2d9a87235596b02f0d9ec8ee303 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c400cb0626fb57f3205798d79619ce6313d30e172bb30ee1561e03c1b0d1c638 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c400cb0626fb57f3205798d79619ce6313d30e172bb30ee1561e03c1b0d1c638->enter($__internal_c400cb0626fb57f3205798d79619ce6313d30e172bb30ee1561e03c1b0d1c638_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_348d5e0c2d6a23dc222be70ede411496949f71a3f055f5f83b41255e44e9ced6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_348d5e0c2d6a23dc222be70ede411496949f71a3f055f5f83b41255e44e9ced6->enter($__internal_348d5e0c2d6a23dc222be70ede411496949f71a3f055f5f83b41255e44e9ced6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_c400cb0626fb57f3205798d79619ce6313d30e172bb30ee1561e03c1b0d1c638->leave($__internal_c400cb0626fb57f3205798d79619ce6313d30e172bb30ee1561e03c1b0d1c638_prof);

        
        $__internal_348d5e0c2d6a23dc222be70ede411496949f71a3f055f5f83b41255e44e9ced6->leave($__internal_348d5e0c2d6a23dc222be70ede411496949f71a3f055f5f83b41255e44e9ced6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
