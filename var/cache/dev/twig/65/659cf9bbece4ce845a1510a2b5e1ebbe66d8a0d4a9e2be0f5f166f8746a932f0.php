<?php

/* @Framework/Form/button_label.html.php */
class __TwigTemplate_0f3d8d6096718cdfb428a6aaea8e6642225e74b09df141aefb5725b7918884a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17b71ae7cc9171ef289eb1fbc737a2e448e804c76b44e59316f53794689e2d8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17b71ae7cc9171ef289eb1fbc737a2e448e804c76b44e59316f53794689e2d8d->enter($__internal_17b71ae7cc9171ef289eb1fbc737a2e448e804c76b44e59316f53794689e2d8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_label.html.php"));

        $__internal_1f9982f28eda6ef55d6e90dfc345fa11df2ed40d42718394766bc3123ba9f876 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f9982f28eda6ef55d6e90dfc345fa11df2ed40d42718394766bc3123ba9f876->enter($__internal_1f9982f28eda6ef55d6e90dfc345fa11df2ed40d42718394766bc3123ba9f876_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_label.html.php"));

        
        $__internal_17b71ae7cc9171ef289eb1fbc737a2e448e804c76b44e59316f53794689e2d8d->leave($__internal_17b71ae7cc9171ef289eb1fbc737a2e448e804c76b44e59316f53794689e2d8d_prof);

        
        $__internal_1f9982f28eda6ef55d6e90dfc345fa11df2ed40d42718394766bc3123ba9f876->leave($__internal_1f9982f28eda6ef55d6e90dfc345fa11df2ed40d42718394766bc3123ba9f876_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_label.html.php";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/button_label.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_label.html.php");
    }
}
