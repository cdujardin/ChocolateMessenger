<?php

/* :Message:completed.html.twig */
class __TwigTemplate_5d4da0e60525fa97151533a87e5a00f82d726b01f58a40f4b8334e1be589832f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Message:completed.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e36471167d394324ca1bd1b36661ad20d8e81cff31468dd861551ff3f4b93d22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e36471167d394324ca1bd1b36661ad20d8e81cff31468dd861551ff3f4b93d22->enter($__internal_e36471167d394324ca1bd1b36661ad20d8e81cff31468dd861551ff3f4b93d22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:completed.html.twig"));

        $__internal_7c004df1cda26d964e756d82e5130943b0b2a529698504390a8a57b14d36457d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c004df1cda26d964e756d82e5130943b0b2a529698504390a8a57b14d36457d->enter($__internal_7c004df1cda26d964e756d82e5130943b0b2a529698504390a8a57b14d36457d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:completed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e36471167d394324ca1bd1b36661ad20d8e81cff31468dd861551ff3f4b93d22->leave($__internal_e36471167d394324ca1bd1b36661ad20d8e81cff31468dd861551ff3f4b93d22_prof);

        
        $__internal_7c004df1cda26d964e756d82e5130943b0b2a529698504390a8a57b14d36457d->leave($__internal_7c004df1cda26d964e756d82e5130943b0b2a529698504390a8a57b14d36457d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b53ced3c83399a3330f78aa8b9b1b70d117838d0de4e9e319cfef184e2453197 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b53ced3c83399a3330f78aa8b9b1b70d117838d0de4e9e319cfef184e2453197->enter($__internal_b53ced3c83399a3330f78aa8b9b1b70d117838d0de4e9e319cfef184e2453197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_78a590933adf188c3cb3691a912a0289bd67d1d04e51dfefdfc4df73b7f3c4c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78a590933adf188c3cb3691a912a0289bd67d1d04e51dfefdfc4df73b7f3c4c7->enter($__internal_78a590933adf188c3cb3691a912a0289bd67d1d04e51dfefdfc4df73b7f3c4c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        <h1 class=\"title\">Completed</h1>
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["messages"] ?? $this->getContext($context, "messages")));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 10
            echo "        <div class=\"col-xs-9\">
          <p class=\"object\">";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["message"], "object", array()), "html", null, true);
            echo "</p>
          <p class=\"message\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["message"], "message", array()), "html", null, true);
            echo "</p>
        </div>
        <div class=\"col-xs-2\">
          <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_show", array("id" => $this->getAttribute($context["message"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/view.png"), "html", null, true);
            echo "\" alt=\"En savoir plus\"/></a>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "      </div>
    </div>

";
        
        $__internal_78a590933adf188c3cb3691a912a0289bd67d1d04e51dfefdfc4df73b7f3c4c7->leave($__internal_78a590933adf188c3cb3691a912a0289bd67d1d04e51dfefdfc4df73b7f3c4c7_prof);

        
        $__internal_b53ced3c83399a3330f78aa8b9b1b70d117838d0de4e9e319cfef184e2453197->leave($__internal_b53ced3c83399a3330f78aa8b9b1b70d117838d0de4e9e319cfef184e2453197_prof);

    }

    public function getTemplateName()
    {
        return ":Message:completed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 18,  73 => 15,  67 => 12,  63 => 11,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        <h1 class=\"title\">Completed</h1>
        {% for message in messages %}
        <div class=\"col-xs-9\">
          <p class=\"object\">{{ message.object }}</p>
          <p class=\"message\">{{ message.message }}</p>
        </div>
        <div class=\"col-xs-2\">
          <a href=\"{{ path('message_show', { 'id': message.id }) }}\"><img src=\"{{ asset('img/view.png') }}\" alt=\"En savoir plus\"/></a>
        </div>
        {% endfor %}
      </div>
    </div>

{% endblock %}
", ":Message:completed.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Message/completed.html.twig");
    }
}
