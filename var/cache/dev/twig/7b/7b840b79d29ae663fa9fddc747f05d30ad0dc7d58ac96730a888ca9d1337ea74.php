<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_aa15f58eb5b23ff33096a330603351099836f7a58ca0137ee1746301f0e7d28e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b535aec8f76365ca06ab42b2da1bc485097374dda42357e2e7fe21dc5bbf5664 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b535aec8f76365ca06ab42b2da1bc485097374dda42357e2e7fe21dc5bbf5664->enter($__internal_b535aec8f76365ca06ab42b2da1bc485097374dda42357e2e7fe21dc5bbf5664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_8d2a7755267022a58c664ef5453f3973465779b080e31a6e0edc7e0687c8cae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d2a7755267022a58c664ef5453f3973465779b080e31a6e0edc7e0687c8cae0->enter($__internal_8d2a7755267022a58c664ef5453f3973465779b080e31a6e0edc7e0687c8cae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_b535aec8f76365ca06ab42b2da1bc485097374dda42357e2e7fe21dc5bbf5664->leave($__internal_b535aec8f76365ca06ab42b2da1bc485097374dda42357e2e7fe21dc5bbf5664_prof);

        
        $__internal_8d2a7755267022a58c664ef5453f3973465779b080e31a6e0edc7e0687c8cae0->leave($__internal_8d2a7755267022a58c664ef5453f3973465779b080e31a6e0edc7e0687c8cae0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
