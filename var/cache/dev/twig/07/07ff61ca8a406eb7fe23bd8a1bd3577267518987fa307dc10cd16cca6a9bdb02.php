<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_5b92c25c5c9b2f32d8bbc27cab807545e111bb03e7b13183f129d14b34485e3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c42e628153f7c8b226409b0c105045c6c8a80d5ecf618b92a0a7f84ef47c8cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c42e628153f7c8b226409b0c105045c6c8a80d5ecf618b92a0a7f84ef47c8cf->enter($__internal_0c42e628153f7c8b226409b0c105045c6c8a80d5ecf618b92a0a7f84ef47c8cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_6e4a4e9c5d77473b1afce90c21f2674936bc9908752899a33145db908ce73e73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e4a4e9c5d77473b1afce90c21f2674936bc9908752899a33145db908ce73e73->enter($__internal_6e4a4e9c5d77473b1afce90c21f2674936bc9908752899a33145db908ce73e73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_0c42e628153f7c8b226409b0c105045c6c8a80d5ecf618b92a0a7f84ef47c8cf->leave($__internal_0c42e628153f7c8b226409b0c105045c6c8a80d5ecf618b92a0a7f84ef47c8cf_prof);

        
        $__internal_6e4a4e9c5d77473b1afce90c21f2674936bc9908752899a33145db908ce73e73->leave($__internal_6e4a4e9c5d77473b1afce90c21f2674936bc9908752899a33145db908ce73e73_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
