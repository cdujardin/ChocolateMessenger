<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_309d080f5974df84644bd4e4095d9e0c79c337c730538a651b6f3c6c15f236eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5c4947a2742907f2bba42e86c00afa1ce1046a57182f9310df56f600c05e980 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5c4947a2742907f2bba42e86c00afa1ce1046a57182f9310df56f600c05e980->enter($__internal_b5c4947a2742907f2bba42e86c00afa1ce1046a57182f9310df56f600c05e980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_5509b1b393be2c38d66c0be03e27218f759a028a6495de3ab909007e657dcec1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5509b1b393be2c38d66c0be03e27218f759a028a6495de3ab909007e657dcec1->enter($__internal_5509b1b393be2c38d66c0be03e27218f759a028a6495de3ab909007e657dcec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_b5c4947a2742907f2bba42e86c00afa1ce1046a57182f9310df56f600c05e980->leave($__internal_b5c4947a2742907f2bba42e86c00afa1ce1046a57182f9310df56f600c05e980_prof);

        
        $__internal_5509b1b393be2c38d66c0be03e27218f759a028a6495de3ab909007e657dcec1->leave($__internal_5509b1b393be2c38d66c0be03e27218f759a028a6495de3ab909007e657dcec1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
