<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_8a72aa64d0b831e00b010149ab4ce976eb1e3850427d11304701fa2c6628539c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf599b3956b677fdaaab2f6750aee59f1bbac77d63e0f9684992f067f17cce6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf599b3956b677fdaaab2f6750aee59f1bbac77d63e0f9684992f067f17cce6e->enter($__internal_cf599b3956b677fdaaab2f6750aee59f1bbac77d63e0f9684992f067f17cce6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_20790cb6bce74c8db218e34f2874ae44324bb4e4bf2369b7a2809cd96f295cfd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20790cb6bce74c8db218e34f2874ae44324bb4e4bf2369b7a2809cd96f295cfd->enter($__internal_20790cb6bce74c8db218e34f2874ae44324bb4e4bf2369b7a2809cd96f295cfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_cf599b3956b677fdaaab2f6750aee59f1bbac77d63e0f9684992f067f17cce6e->leave($__internal_cf599b3956b677fdaaab2f6750aee59f1bbac77d63e0f9684992f067f17cce6e_prof);

        
        $__internal_20790cb6bce74c8db218e34f2874ae44324bb4e4bf2369b7a2809cd96f295cfd->leave($__internal_20790cb6bce74c8db218e34f2874ae44324bb4e4bf2369b7a2809cd96f295cfd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
