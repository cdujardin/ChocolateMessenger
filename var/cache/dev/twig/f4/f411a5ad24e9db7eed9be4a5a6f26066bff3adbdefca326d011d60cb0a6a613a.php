<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_0798e37c38d513bea7b709472f00287adf735e9290f5e482edd74f4ecf2ed013 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1d0af9bcd8387c3ae4cc3da4a2ea23d843f80fd265eb660870401fae5fb1847 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1d0af9bcd8387c3ae4cc3da4a2ea23d843f80fd265eb660870401fae5fb1847->enter($__internal_d1d0af9bcd8387c3ae4cc3da4a2ea23d843f80fd265eb660870401fae5fb1847_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_4c6b7d6e468ec81f1f729cdf6745f30f894789c7dda6c06e1e72d5e275fd7ba3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c6b7d6e468ec81f1f729cdf6745f30f894789c7dda6c06e1e72d5e275fd7ba3->enter($__internal_4c6b7d6e468ec81f1f729cdf6745f30f894789c7dda6c06e1e72d5e275fd7ba3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_d1d0af9bcd8387c3ae4cc3da4a2ea23d843f80fd265eb660870401fae5fb1847->leave($__internal_d1d0af9bcd8387c3ae4cc3da4a2ea23d843f80fd265eb660870401fae5fb1847_prof);

        
        $__internal_4c6b7d6e468ec81f1f729cdf6745f30f894789c7dda6c06e1e72d5e275fd7ba3->leave($__internal_4c6b7d6e468ec81f1f729cdf6745f30f894789c7dda6c06e1e72d5e275fd7ba3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
