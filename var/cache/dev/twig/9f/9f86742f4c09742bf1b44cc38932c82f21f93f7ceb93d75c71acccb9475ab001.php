<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_d09f5d001bdfae6a908da2fca527dcc57f94534cbb2a2fd4d8c1f862b601f725 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b4181bed0642f4c37775e1d0d08b0da80d8427fedc200dadb17d67b11e01549 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b4181bed0642f4c37775e1d0d08b0da80d8427fedc200dadb17d67b11e01549->enter($__internal_5b4181bed0642f4c37775e1d0d08b0da80d8427fedc200dadb17d67b11e01549_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_530d4f305c52823135306b1499d669b7d55658d2db6004dc3659838496cb5ac2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_530d4f305c52823135306b1499d669b7d55658d2db6004dc3659838496cb5ac2->enter($__internal_530d4f305c52823135306b1499d669b7d55658d2db6004dc3659838496cb5ac2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b4181bed0642f4c37775e1d0d08b0da80d8427fedc200dadb17d67b11e01549->leave($__internal_5b4181bed0642f4c37775e1d0d08b0da80d8427fedc200dadb17d67b11e01549_prof);

        
        $__internal_530d4f305c52823135306b1499d669b7d55658d2db6004dc3659838496cb5ac2->leave($__internal_530d4f305c52823135306b1499d669b7d55658d2db6004dc3659838496cb5ac2_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_636ed7048535f8500660fccca24457fa21e036a83da0b890fc1c28284d07c37a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_636ed7048535f8500660fccca24457fa21e036a83da0b890fc1c28284d07c37a->enter($__internal_636ed7048535f8500660fccca24457fa21e036a83da0b890fc1c28284d07c37a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_d2c3d9da00ac8ff32474223b246263ccb492f9cc41c3b672ad05a33030f7449f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2c3d9da00ac8ff32474223b246263ccb492f9cc41c3b672ad05a33030f7449f->enter($__internal_d2c3d9da00ac8ff32474223b246263ccb492f9cc41c3b672ad05a33030f7449f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_d2c3d9da00ac8ff32474223b246263ccb492f9cc41c3b672ad05a33030f7449f->leave($__internal_d2c3d9da00ac8ff32474223b246263ccb492f9cc41c3b672ad05a33030f7449f_prof);

        
        $__internal_636ed7048535f8500660fccca24457fa21e036a83da0b890fc1c28284d07c37a->leave($__internal_636ed7048535f8500660fccca24457fa21e036a83da0b890fc1c28284d07c37a_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_775d78951c6245d9c43955122909e0095888c9a20340f056a7e692aefb9a0b1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_775d78951c6245d9c43955122909e0095888c9a20340f056a7e692aefb9a0b1f->enter($__internal_775d78951c6245d9c43955122909e0095888c9a20340f056a7e692aefb9a0b1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_fdb0db66b18507cec8c5bdc3368f1d54259fed9b2c7d462397b65c96763a7c1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdb0db66b18507cec8c5bdc3368f1d54259fed9b2c7d462397b65c96763a7c1b->enter($__internal_fdb0db66b18507cec8c5bdc3368f1d54259fed9b2c7d462397b65c96763a7c1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_fdb0db66b18507cec8c5bdc3368f1d54259fed9b2c7d462397b65c96763a7c1b->leave($__internal_fdb0db66b18507cec8c5bdc3368f1d54259fed9b2c7d462397b65c96763a7c1b_prof);

        
        $__internal_775d78951c6245d9c43955122909e0095888c9a20340f056a7e692aefb9a0b1f->leave($__internal_775d78951c6245d9c43955122909e0095888c9a20340f056a7e692aefb9a0b1f_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
