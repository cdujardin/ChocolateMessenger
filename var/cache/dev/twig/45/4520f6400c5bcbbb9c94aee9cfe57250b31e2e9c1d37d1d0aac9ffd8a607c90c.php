<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_c68e8f3556d491c6a7920c412510dd2829b44d6b0957808f7a1412d0e2a0a883 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_874dfddf31a6f3cc4a24226f78bb5feb95d5e84edd9f15d2ca89a886ee73527c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_874dfddf31a6f3cc4a24226f78bb5feb95d5e84edd9f15d2ca89a886ee73527c->enter($__internal_874dfddf31a6f3cc4a24226f78bb5feb95d5e84edd9f15d2ca89a886ee73527c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_6723b181f2de74f343e375ea74056f37c9738a5e935c8b4b5e2f1253c8e7b538 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6723b181f2de74f343e375ea74056f37c9738a5e935c8b4b5e2f1253c8e7b538->enter($__internal_6723b181f2de74f343e375ea74056f37c9738a5e935c8b4b5e2f1253c8e7b538_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_874dfddf31a6f3cc4a24226f78bb5feb95d5e84edd9f15d2ca89a886ee73527c->leave($__internal_874dfddf31a6f3cc4a24226f78bb5feb95d5e84edd9f15d2ca89a886ee73527c_prof);

        
        $__internal_6723b181f2de74f343e375ea74056f37c9738a5e935c8b4b5e2f1253c8e7b538->leave($__internal_6723b181f2de74f343e375ea74056f37c9738a5e935c8b4b5e2f1253c8e7b538_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
