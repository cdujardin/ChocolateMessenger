<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_130f1aae5fb1f2edca868dca51bc325092dc39c0a8ac85fe6a9574f7ab9718a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0047a0d89252670d8217d4c65fe89a3411d71ae2217782ead41be6899c693083 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0047a0d89252670d8217d4c65fe89a3411d71ae2217782ead41be6899c693083->enter($__internal_0047a0d89252670d8217d4c65fe89a3411d71ae2217782ead41be6899c693083_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_502006e43ba63220b395ea829a157f33ed38ebfb59b8518e52450b96c62f0dfd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_502006e43ba63220b395ea829a157f33ed38ebfb59b8518e52450b96c62f0dfd->enter($__internal_502006e43ba63220b395ea829a157f33ed38ebfb59b8518e52450b96c62f0dfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_0047a0d89252670d8217d4c65fe89a3411d71ae2217782ead41be6899c693083->leave($__internal_0047a0d89252670d8217d4c65fe89a3411d71ae2217782ead41be6899c693083_prof);

        
        $__internal_502006e43ba63220b395ea829a157f33ed38ebfb59b8518e52450b96c62f0dfd->leave($__internal_502006e43ba63220b395ea829a157f33ed38ebfb59b8518e52450b96c62f0dfd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
